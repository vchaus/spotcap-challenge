package controllers

// Basic UI controllers

import "net/http"

var LoginHandler = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "text/html")
	http.ServeFile(writer, request, "views/index.html")
})

var HomeHandler = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "text/html")
	http.ServeFile(writer, request, "views/home.html")
})