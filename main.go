package main

import (
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	api "spotcapApi/api"
	ui "spotcapApi/controllers"
	"strconv"
)

func main() {
	port := 8000
	router := mux.NewRouter()

	// Routing for UI and static files
	router.Handle("/", ui.LoginHandler)
	router.Handle("/home", ui.HomeHandler)
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	// Authentication
	router.HandleFunc("/login", AuthHandler).Methods("POST")
	router.HandleFunc("/signin", UISignInHandler).Methods("POST")

	// API
	router.Handle("/users", JWTMiddleware.Handler(api.GetAllUsers)).Methods("GET")
	router.Handle("/users", JWTMiddleware.Handler(api.AddUser)).Methods("POST")
	router.Handle("/users/{id}", JWTMiddleware.Handler(api.GetUser)).Methods("GET")
	router.Handle("/users/{id}", JWTMiddleware.Handler(api.UpdateUser)).Methods("PUT")
	router.Handle("/users/{id}", JWTMiddleware.Handler(api.DeleteUser)).Methods("DELETE")

	http.ListenAndServe(":"+strconv.Itoa(port), handlers.LoggingHandler(os.Stdout, router))
}
