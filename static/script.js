// Simple client side functions to work with the API: show the list of users and perform CRUD operations

window.addEventListener('DOMContentLoaded', function (event) {
    document.getElementById("jwt-token").innerText = getCookie("jwt-token");
    getUsers()
});

function getCookie(name) {
    let cookies = RegExp(name + "=[^;]+").exec(document.cookie);
    return decodeURIComponent(!!cookies ? cookies.toString().replace(/^[^=]+./, "") : "");
}

function sendSignedRequest(method, url, data, expectedStatus, cb) {
    let request = new XMLHttpRequest();
    request.open(method, url, true);
    request.setRequestHeader("Authorization", "Bearer " + getCookie("jwt-token"));
    request.setRequestHeader("Content-Type", "application/json");
    request.send(data);
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == expectedStatus) {
            cb(request.responseText)
        }
    }
}

// Build a user object from the form and submit in signed request
// TODO: add data validation
function addUser() {
    let user = {
        amount: parseInt(document.getElementById("amount").value),
        incdate: document.getElementById("incdate").value,
        purpose: document.getElementById("purpose").value,
        revenue: parseInt(document.getElementById("revenue").value),
        industry: document.getElementById("industry").value,
    }
    sendSignedRequest("POST", "/users", JSON.stringify(user), 200, function (res) {
        location.reload();
    })
}

function getUser(id) {
    sendSignedRequest("GET", "/users/" + id, {}, 200, function (res) {
        let user = JSON.parse(res);
        document.getElementById("id-update").value = user.userid;
        document.getElementById("amount-update").value = user.amount;
        document.getElementById("incdate-update").value = user.incdate;
        document.getElementById("purpose-update").value = user.purpose;
        document.getElementById("revenue-update").value = user.revenue;
        document.getElementById("industry-update").value = user.industry;
        document.getElementById("btn-show-modal").click();
    })
}

function updateUser() {
    let user = {
        amount: parseInt(document.getElementById("amount-update").value),
        incdate: document.getElementById("incdate-update").value,
        purpose: document.getElementById("purpose-update").value,
        revenue: parseInt(document.getElementById("revenue-update").value),
        industry: document.getElementById("industry-update").value,
    }
    let userId = document.getElementById("id-update").value;

    sendSignedRequest("PUT", "/users/" + userId, JSON.stringify(user), 200, function (res) {
        location.reload();
    })
}

function deleteUser(id) {
    sendSignedRequest("DELETE", "/users/" + id, {}, 202, function (res) {
        location.reload();
    })
}

// Get the list of users from the API and show on the page
function getUsers() {
    sendSignedRequest("GET", "/users", {}, 200, function (res) {
        let users = JSON.parse(res);
        updateUI(users)
    })
}

function updateUI(users) {
    users.forEach(function (user, ind) {
        // For each user create a row in the table and set appropriate event listeners
        let row = document.createElement('div');
        row.id = 'row' + ind;
        row.className = 'row';

        // Update button
        let cellUpd = document.createElement('div');
        cellUpd.className = 'col-sm col-action';
        let buttonUpd = document.createElement('button');
        buttonUpd.innerText = "Update";
        buttonUpd.type = "button";
        buttonUpd.className = "btn btn-warning btn-sm";
        buttonUpd.addEventListener("click", function (e) {
            getUser(user.userid)
        }, false);
        cellUpd.appendChild(buttonUpd);
        row.appendChild(cellUpd);

        // User data
        for (let key in user) {
            let cell = document.createElement('div');
            cell.className = 'col-sm';
            cell.innerText = user[key];
            row.appendChild(cell);
        }

        // Delete button
        let cellDel = document.createElement('div');
        cellDel.className = 'col-sm col-action';
        let buttonDel = document.createElement('button');
        buttonDel.innerText = "Delete";
        buttonDel.type = "button";
        buttonDel.className = "btn btn-danger btn-sm";
        buttonDel.addEventListener("click", function (e) {
            deleteUser(user.userid)
        }, false);
        cellDel.appendChild(buttonDel);
        row.appendChild(cellDel);

        let separator = document.createElement('hr');
        separator.className = "my-4";
        document.getElementById('table-users').appendChild(row);
        document.getElementById('table-users').appendChild(separator);
    })
}