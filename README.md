# Spotcap PM Challenge

A simple [go](http://www.golang.org) app that handles UI and API for the Spotcap challenge. 

The app is hosted at [spotcap-challenge.tech](http://spotcap-challenge.tech/)   

Requirements and Postman tests are available in the `docs` directory.

See [the API docs](https://app.swaggerhub.com/apis-docs/vchaus/Spotcap-challenge/1.0.0) for examples of usage.
 
 
## How to run locally

Get dependencies:
````
go get github.com/mongodb/mongo-go-driver 
go get github.com/gorilla/mux
go get github.com/gorilla/handlers
go get github.com/dgrijalva/jwt-go
go get github.com/auth0/go-jwt-middleware
````

Make sure that [Mongo DB]() is running on the default port: `mongodb://127.0.0.1:27017`

Build and install the app:

`go install spotcap-challenge`

Run the app:

`spotcap-challenge`

The web server will start on the port 8000. 
You can start sending requests to the API or access the UI at [localhost:8000](http://localhost:8000) 

Depending on your local `go` set up you might need to copy `views`, `static` and `spotcap-keys` folders 
to the same directory where your `spotcap-challenge` binaries stored.  