package api

// A simple data access layer that handles CRUD operations for the users collection

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	models "spotcapApi/models"
	"time"
)

const DBURI = "mongodb://127.0.0.1:27017/" // URI for the database. It's hosted locally, not accessible from the outside.
const DBNAME = "spotcap"
const COLLECTIONNAME = "users"
const CONNECTIONTIMEOUT = 10 * time.Second

var db *mongo.Database

// Initialize db connection once at launch
func init() {
	ctx, _ := context.WithTimeout(context.Background(), CONNECTIONTIMEOUT)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(DBURI))
	if err != nil {
		log.Fatal(err)
	}

	db = client.Database(DBNAME)
}

// Insert a user into the collection and return a newly created user id
func InsertUser(user models.NewUser) string {
	var insertId string
	res, err := db.Collection(COLLECTIONNAME).InsertOne(context.Background(), user)
	if err != nil {
		log.Print(err)
	} else {
		insertId = res.InsertedID.(primitive.ObjectID).Hex()
	}
	return insertId
}

// Id is a hexadecimal string for simplicity
func GetUserById(id string) models.User {
	var user models.User
	objId, _ := primitive.ObjectIDFromHex(id)
	if err := db.Collection(COLLECTIONNAME).FindOne(context.Background(), bson.M{"_id": objId}).Decode(&user); err != nil {
		log.Printf("Error: %s. Requested user id: %s", err, id)
	}
	return user
}

func UpdateUserById(id string, user models.User) int {
	objId, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": bson.M{"$eq": objId}}
	update := bson.M{
		"$set": bson.M{
			"amount":   user.Amount,
			"incdate":  user.IncDate,
			"purpose":  user.Purpose,
			"industry": user.Industry,
			"revenue":  user.Revenue,
		},
	}
	res, err := db.Collection(COLLECTIONNAME).UpdateOne(context.Background(), filter, update)
	if err != nil {
		log.Printf("Error: %s. Requested user id: %s. user: ", err, id, user)
	}
	return int(res.MatchedCount)
}

// Delete the user
// Basic implementation, in real world scenario it's worth flagging user as deleted instead
func DeleteUserById(id string) int {
	objId, _ := primitive.ObjectIDFromHex(id)
	res, err := db.Collection(COLLECTIONNAME).DeleteOne(context.Background(), bson.M{"_id": objId})
	if err != nil {
		log.Printf("Error: %s. Requested user id: %s", err, id)
	}
	return int(res.DeletedCount)
}

// Get all users from the collection
func GetUsers() models.Users {
	usersCollection, err := db.Collection(COLLECTIONNAME).Find(context.Background(), bson.D{{}})
	if err != nil {
		log.Fatal(err)
	}
	var users models.Users
	var user models.User
	for usersCollection.Next(context.Background()) {
		err := usersCollection.Decode(&user)
		if err != nil {
			log.Fatal(err)
		}
		users = append(users, user)
	}
	if err := usersCollection.Err(); err != nil {
		log.Fatal(err)
	}
	usersCollection.Close(context.Background())
	return users
}