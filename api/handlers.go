package api

// API calls handlers for basic CRUD operations

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	models "spotcapApi/models"
)

var GetAllUsers = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
	Users := GetUsers()
	if Users != nil {
		writer.Header().Set("Content-Type", "application/json; charset=UTF-8")
		writer.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(writer).Encode(Users); err != nil {
			panic(err)
		}
	} else {
		writer.WriteHeader(http.StatusNoContent)
	}
})

var AddUser = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
	var user models.NewUser
	err := json.NewDecoder(request.Body).Decode(&user)
	if err != nil {
		fmt.Print(err)
	}

	userId := InsertUser(user)
	if userId != "" {
		user.Id = userId
		fmt.Print(user)

		writer.Header().Set("Content-Type", "application/json; charset=UTF-8")
		writer.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(writer).Encode(user); err != nil {
			panic(err)
		}
	} else {
		writer.WriteHeader(http.StatusBadRequest)
	}
})

var GetUser = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	userId := vars["id"]
	user := GetUserById(userId)

	writer.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if user == (models.User{}) {
		writer.WriteHeader(http.StatusNotFound)
	} else {
		writer.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(writer).Encode(user); err != nil {
			panic(err)
		}
	}
})

var UpdateUser = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
	userId := mux.Vars(request)["id"]
	var user models.User
	_ = json.NewDecoder(request.Body).Decode(&user)
	user.Id = userId

	updateCount := UpdateUserById(userId, user)
	if updateCount == 0 {
		writer.WriteHeader(http.StatusNotFound)
	} else {
		writer.Header().Set("Content-Type", "application/json; charset=UTF-8")
		writer.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(writer).Encode(user); err != nil {
			panic(err)
		}
	}
})

var DeleteUser = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
	userId := mux.Vars(request)["id"]
	deleteCount := DeleteUserById(userId)
	if deleteCount == 0 {
		writer.WriteHeader(http.StatusNotFound)
	} else {
		writer.WriteHeader(http.StatusAccepted)
	}
})