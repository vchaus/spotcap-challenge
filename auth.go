package main

import (
	"crypto/rsa"
	"fmt"
	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"io/ioutil"
	"net/http"
	"time"
)

// RSA keys
const PRIVATEKEYPATH = "spotcap-keys/private-key"
const PUBLICKEYPATH = "spotcap-keys/public-key.pub"

var verifyKey *rsa.PublicKey
var signKey *rsa.PrivateKey

// Custom types for JWT tokens
type CustomerInfo struct {
	Name string
	Kind string
}

type SimpleClaim struct {
	*jwt.StandardClaims
	TokenType string
	CustomerInfo
}

// Read RSA keys at start
func init() {
	signBytes, err := ioutil.ReadFile(PRIVATEKEYPATH)
	fatal(err)
	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	fatal(err)
	verifyBytes, err := ioutil.ReadFile(PUBLICKEYPATH)
	fatal(err)
	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	fatal(err)
}

// Simple login handler
// Checks that login/pass matches and returns JWT token
func AuthHandler(w http.ResponseWriter, r *http.Request) {
	user := r.FormValue("email")
	pass := r.FormValue("pass")

	// Basic login check, for test purposes only
	// In real world app it's going to be authenticated through third party or from DB with hashed password with salt
	if user != "lillian.martin@spotcap.com" || pass != "At9b&CSn!mYT3kP28%f" {
		w.WriteHeader(http.StatusForbidden)
			fmt.Fprintln(w, "Invalid email or password.")
		return
	}
	tokenString, _ := createToken(user)
	w.Header().Set("Content-Type", "application/jwt")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, tokenString)
}

// A UI copy of the login handler for simplicity
// Sets a JWT cookie and redirects to the home page
func UISignInHandler(w http.ResponseWriter, r *http.Request) {
	user := r.FormValue("email")
	pass := r.FormValue("pass")
	if user != "lillian.martin@spotcap.com" || pass != "At9b&CSn!mYT3kP28%f" {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintln(w, "Invalid email or password.")
		return
	}
	tokenString, _ := createToken(user)
	cookie := http.Cookie{
		Name:  "jwt-token",
		Value: tokenString,
		Path:  "/",
	}
	http.SetCookie(w, &cookie)
	http.Redirect(w, r, "/home", http.StatusSeeOther)
}

// Generate and sign a token
func createToken(user string) (string, error) {
	t := jwt.New(jwt.GetSigningMethod("RS256"))
	t.Claims = &SimpleClaim{&jwt.StandardClaims{ExpiresAt: time.Now().Add(time.Hour * 96).Unix()},
		"level1", CustomerInfo{user, "admin"},
	}
	return t.SignedString(signKey)
}

// JWT middleware to wrap each request that has to be secured
// It verifies if request is signed with a valid token
var JWTMiddleware = jwtmiddleware.New(jwtmiddleware.Options{
	ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
		return verifyKey, nil
	},
	SigningMethod: jwt.SigningMethodRS256,
})