package models

type NewUser struct {
	Amount   int    `json:"amount"`
	IncDate  string `json:"incdate"`
	Purpose  string `json:"purpose"`
	Industry string `json:"industry"`
	Revenue  int    `json:"revenue"`
	Id       string `json:"userid"`
}

// A copy model for easier bson decoding
// TODO: both should be merged into one
type User struct {
	Amount   int    `json:"amount"`
	IncDate  string `json:"incdate"`
	Purpose  string `json:"purpose"`
	Industry string `json:"industry"`
	Revenue  int    `json:"revenue"`
	Id       string `json:"userid" bson:"_id"`
}

type Users []User
